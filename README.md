_Please note: Please use Laravel with exercises where it makes sense._

## Basic CS

Solve exercises A,B,C,D and explain your reasoning. It's ok to use pseudo code where needed. Or any popular language you're comfortable with (except if noted otherwise).

A) Design a SQL database to store NBA players, teams and games (column and table contents are all up to you). Users mostly query game results by date and team name. The second most frequent query is players statistics by player name

   - See folder BasicCS/A
   - Also I make index on player name and team name columns  
   
B) How would you find files that begin with "0aH" and delete them given a folder (with subfolders)? Assume there are many files in the folder.

     find  . -name '0aH*' -exec rm {} \;
   
C) Write a function that sorts 11 small numbers (<100) as fast as possible. Estimate how long it would take to execute that function 10 Billion (10^10) times on a normal machine?

   - We have many Algorithms for sort:     
       - Bobble sort
       - Insertion sort
       - Selection sort
       - Quick sort
       - Merge Sort... 
        
        
   - For this task I chose Insertion Sort Algorithms because we use only array of 11 numbers:
   - It is one of the elementary sorting algorithms with O(n2) worst-case time, insertion sort is the algorithm of choice either when the data is nearly sorted (because it is adaptive) or when the problem size is small (because it has low overhead).
        
        ```
        function insertionSort($data)
        {
            $n=count($data);
            $next=null;
            for($i=1; $i<$n; $i++)//outer loop
            {
                $next=$data[$i];
                for($j=$i-1; $j>=0; $j--)//inner loop
                {
                    if( $data[$j]>$next )//change > to < for descending order
                    {
                        $data[$j+1]=$data[$j];
                    }
                    else
                    {
                        break;
                    }
                }
                $data[$j+1]=$next; // insert the next value to the correct postion of the already sorted elements
         
            }
         
            return $data;
        }
        ```
           
D) Write a function that sorts 10000 powers (a^b) where a and b are random numbers between 100 and 10000? Estimate how long it would take on your machine?

- For this task I chose Quick Sort Algorithms. 
- Execution time O(n2), but typically O(n·lg(n)) time

        ```
        function partition(&$arr,$leftIndex,$rightIndex)
        {
            $pivot=$arr[($leftIndex+$rightIndex)/2];
             
            while ($leftIndex <= $rightIndex) 
            {        
                while ($arr[$leftIndex] < $pivot)             
                        $leftIndex++;
                while ($arr[$rightIndex] > $pivot)
                        $rightIndex--;
                if ($leftIndex <= $rightIndex) {  
                        $tmp = $arr[$leftIndex];
                        $arr[$leftIndex] = $arr[$rightIndex];
                        $arr[$rightIndex] = $tmp;
                        $leftIndex++;
                        $rightIndex--;
                }
            }
            echo implode(",",$arr)." @pivot $pivot<br>";
            return $leftIndex;
        }
         
        function quickSort(&$arr, $leftIndex, $rightIndex)
        {
            $index = partition($arr,$leftIndex,$rightIndex);
            if ($leftIndex < $index - 1)
                quickSort($arr, $leftIndex, $index - 1);
            if ($index < $rightIndex)
                quickSort($arr, $index, $rightIndex);
        }
        $nums = array(5,3,8,6,2,7);
        quickSort($nums,0,count($nums)-1);
        ```

## Advanced/Practical

See Readme file in carthook_api_test_app/ folder