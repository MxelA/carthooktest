<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['middleware' => ['api'], 'prefix' => 'v1', 'namespace' => 'APIv1'], function()
{
    Route::get('users',  ['as' => 'api.v1.users', 'uses' => 'UserController@index']);
    Route::get('user/{userId}',  ['as' => 'api.v1.user', 'uses' => 'UserController@show']);
    Route::get('user/{userId}/posts',  ['as' => 'api.v1.user.posts', 'uses' => 'UserController@userPosts']);

    Route::get('posts',  ['as' => 'api.v1.posts', 'uses' => 'PostController@index']);
    Route::get('post/{postId}',  ['as' => 'api.v1.user.post', 'uses' => 'PostController@show']);
    Route::get('post/{postId}/comments',  ['as' => 'api.v1.user.post.comment', 'uses' => 'PostController@postComments']);

    Route::get('comments',  ['as' => 'api.v1.comments', 'uses' => 'CommentController@index']);
    Route::get('comment/{commentId}',  ['as' => 'api.v1.comment', 'uses' => 'CommentController@show']);
});
