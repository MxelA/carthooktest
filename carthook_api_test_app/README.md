## Requirements

- PHP 7.2+
- [Composer](https://getcomposer.org/download)


## Setup notes


- Copy .env.example to .env
- Setup DB credentials in .env file
- Run: composer install
- Run: php artisan migrate
- Run: php artisan serve
- Run: php artisan import:external-clients
- Run: php artisan l5-swagger:generate

## Note Setup Cron
- Run: crontab -e
- Paste: * * * * * cd <location-of-project> && php artisan schedule:run >> /dev/null 2>&1
- Save document

## API Documentation
- http://127.0.0.1:8000/api/documentation

> If you not setup cron for running game, then you will start manual game with command:

## Summary

- First thing you must import data from JSONPlaceHolder third part service. You can do it by running command
in a setup note. Command run the laravel queue job that import data. In default laravel setup, queue is in sync mode,
so if you want run job async you must setup laravel queue. 
Queue name for import data is "import-clients" 

- Like you can see I make service for pulling data that extends abstract class ThirdPartClients. I did this so 
I could easy add another third part service to pull data.

- In migration files you can see columns like client_type, client_id, post_type, post_id...
These columns serve to know for which service the data was retrieved. 
I also made some indexes in DB for faster querying data.

- I use swagger package for build API documentation.

- I have file "Test CartHook API.postman_collection.json" so you can import API endpoints to Postman. 

### Note 
I did not make php unit test for application. I did't not have enough time.


