<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ClientPostComment extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_id', 'name', 'email',
    ];


    /***************
     * RELATIONS
     **************/
    public function post()
    {
        return $this->belongsTo(ClientPost::class);
    }

    /******************
     * SCOPES
     ******************/
    public function scopeCriteriaByName($query, string $name = null)
    {
        if($name) {
            return $query->where('name', $name);
        }
    }

    public function scopeCriteriaByEmail($query, string $email = null)
    {
        if($email) {
            return $query->where('name', $email);
        }
    }
}
