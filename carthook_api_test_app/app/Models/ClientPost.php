<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class ClientPost extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id', 'title', 'body',
    ];


    /***************
     * RELATIONS
     **************/
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /******************
     * SCOPES
     ******************/
    public function scopeCriteriaByTitle($query, string $title = null)
    {
        if($title) {
            return $query->where('title', 'LIKE', $title . '%');
        }
    }

    public function scopeCriteriaByClient($query, Client $client = null)
    {
        if($client) {
            return $query->where('client_id', $client->id);
        }
    }
}
