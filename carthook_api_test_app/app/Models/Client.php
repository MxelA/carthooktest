<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Client extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'username',
    ];

    public function scopeCriteriaByEmail($query, string $email = null)
    {
        if($email) {
            return $query->where('email', $email);
        }
    }

    public function scopeCriteriaByName($query, string $name = null)
    {
        if($name) {
            return $query->where('name','LIKE', '%'. $name . '%');
        }
    }
}
