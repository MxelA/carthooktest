<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar Milic
 * Date: 5/27/20
 * Time: 9:42 PM
 * email: milicalex@gmail.com
 */
namespace App\Services\ThirdPartClients;

abstract class ThirdPartClients implements IThirdPartClients
{
    protected $baseUrl;
    protected $clientService;

    public function __construct(string $baseUrl = null)
    {
        if($baseUrl) {
            $this->baseUrl = $baseUrl;
        }

        if(!$this->baseUrl) {
            throw new Exception('Base URL not set!');
        }

        $this->clientService = new \GuzzleHttp\Client([
            'base_uri' => $this->baseUrl,
            'headers' => [
                'Accept' => 'application/json'
            ]
        ]);
    }

}