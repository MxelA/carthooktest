<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar Milic
 * Date: 5/27/20
 * Time: 9:41 PM
 * email: milicalex@gmail.com
 */

namespace App\Services\ThirdPartClients;

interface IThirdPartClients
{
    public function getUsers(array $params) :array;

    public function getPosts(array $params) :array;

    public function getComments(array $params = []) :array;
}