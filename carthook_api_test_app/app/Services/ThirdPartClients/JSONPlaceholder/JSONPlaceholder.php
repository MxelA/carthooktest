<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar Milic
 * Date: 5/27/20
 * Time: 9:49 PM
 * email: milicalex@gmail.com
 */

namespace App\Services\ThirdPartClients\JSONPlaceholder;

use App\Services\ThirdPartClients\ThirdPartClients;

class JSONPlaceholder extends ThirdPartClients
{
    protected $baseUrl = 'https://jsonplaceholder.typicode.com';

    public function getUsers(array $params = []) :array
    {

        $request = $this->clientService->get('/users', [
            'headers' => [
                'Accept' => 'application/json'
            ],
            'query'   => $params
        ]);

        return (array) \GuzzleHttp\json_decode((string) $request->getBody()->getContents());
    }

    public function getPosts(array $params = []) :array
    {
        $request = $this->clientService->get('/posts', [
            'headers' => [
                'Accept' => 'application/json'
            ],
            'query'   => $params
        ]);

        return (array) \GuzzleHttp\json_decode((string) $request->getBody()->getContents());
    }

    public function getComments(array $params = []) :array
    {
        $request = $this->clientService->get('/comments', [
            'headers' => [
                'Accept' => 'application/json'
            ],
            'query'   => $params
        ]);

        return (array) \GuzzleHttp\json_decode((string) $request->getBody()->getContents());
    }
}