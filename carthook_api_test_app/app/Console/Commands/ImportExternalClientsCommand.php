<?php

namespace App\Console\Commands;

use App\Jobs\ImportJSONPlaceholderClientsJob;
use Illuminate\Console\Command;

class ImportExternalClientsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:external-clients';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Import JSON Placeholder data');
        ImportJSONPlaceholderClientsJob::dispatch()->onQueue('import-clients');
    }
}
