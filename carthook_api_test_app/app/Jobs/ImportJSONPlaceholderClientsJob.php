<?php

namespace App\Jobs;

use App\Models\Client;
use App\Models\ClientPost;
use App\Models\ClientPostComment;
use App\Services\ThirdPartClients\JSONPlaceholder\JSONPlaceholder;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class ImportJSONPlaceholderClientsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $users;
    protected $posts;
    protected $comments;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->users = collect([]);
        $this->posts = collect([]);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $userForInsert = collect([]);

        try{
            dump('Fetching User Data from API-a');
            $users = collect((new JSONPlaceholder())->getUsers());

        } catch (\Exception $e) {
            report($e);
        }

        foreach ($users as $user)
        {
            $user = (array) $user;

            $userData = $this->validateUserData($user);

            if($userData) {
                $this->users->push($userData);
                $userForInsert->push($userData);
            }
        }

        dump('Insert User Data from API-a');
        Client::insert($userForInsert->toArray());



        $this->importPosts();
        $this->importComments();
    }


    /**
     * Import Posts in DB from JsonPlaceholder API-a
     */
    private function importPosts()
    {

        if($this->users->isNotEmpty())
        {
            try{
                //Get All post then filer on application level because API is slow for multiple calls
                dump('Fetching Post Data from API-a');
                $posts = collect((new JSONPlaceholder())->getPosts());
                dump('Error Fetching Post from API-a');
            } catch (\Exception $e) {
                report($e);
                return 0;
            }

            $postsForInsert = collect([]);

            foreach ($this->users as $user)
            {

                $userPosts = $posts->where('userId', $user['client_id']);

                foreach ($userPosts as $post)
                {
                    $client = Client::where('client_type', JSONPlaceholder::class)
                        ->where('client_id', $user['client_id'])
                        ->first()
                    ;

                    if($client) {
                        $postData = $this->validatePostData($client, (array) $post);

                        if($postData)
                        {
                            $postsForInsert->push($postData);
                            $this->posts->push($postData);
                        }
                    }
                }
            }

            dump('Insert Post Data from API-a');
            ClientPost::insert($postsForInsert->toArray());
        }
    }

    /**
     * Import comments in DB from JsonPlaceholder API
     */
    private function importComments()
    {
        try{
            //Get Post comments.
            dump('Fetching Comments from API-a');
            $comments = collect((new JSONPlaceholder())->getComments());
        } catch (\Exception $e) {
            report($e);
            dump('Error Fetching Comments from API-a');
            return 0;
        }

        $commentsForInsert = collect([]);
        foreach ($this->posts as $post)
        {
            foreach ($comments as $comment)
            {
                $post = ClientPost::where('post_type', JSONPlaceholder::class)
                    ->where('post_id', $post['post_id'])
                    ->first()
                ;

                if($post)
                {
                    $commentsData = $this->validateCommentData($post, (array) $comment);

                    if($commentsData)
                    {
                        $commentsForInsert->push($commentsData);
                    }
                }

            }
        }

        dump('Insert Post Comments from API-a');
        ClientPostComment::insert($commentsForInsert->toArray());
    }


    /**
     * Validate Comment Data from API
     * @param ClientPost $post
     * @param array $comment
     * @return array|null
     */
    private function validateCommentData(ClientPost $post, array $comment): ?array
    {
        $validator = Validator::make($comment, [
            'id' => [
                'required',
                'integer',
                Rule::unique('client_post_comments')->where(function ($query) use($post) {
                    return $query->where('comment_type', JSONPlaceholder::class)
                        ->where('post_id', $post['id'])
                        ;
                }),
            ],
            'name'      => 'required|max:100',
            'email'     => 'required|email',
            'body'      => 'required',
        ]);


        $commentData = [
            'post_id'     => $post->id,
            'comment_id'  => $comment['id']?? null,
            'comment_type'=> JSONPlaceholder::class,
            'name'        => $comment['name']?? null,
            'body'        => $comment['body']?? null,
            'email'       => $comment['email']?? null,
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ];

        if($validator->fails())
        {
            return null;
        }

        return $commentData;
    }


    /**
     * Validate Post data from API
     * @param Client $client
     * @param array $post
     * @return array|null
     */
    private function validatePostData(Client $client, array $post): ?array
    {
        $validator = Validator::make($post, [
            'id'        => [
                'required',
                'integer',
                Rule::unique('client_posts')->where(function ($query) use($post) {
                    return $query->where('post_type', JSONPlaceholder::class)
                        ->where('post_id', $post['id'])
                    ;
                }),
            ],
            'userId'    => 'required|integer',
            'title'     => 'required|max:100',
            'body'      => 'required',
        ]);


        $postData = [
            'client_id'   => $client->id,
            'post_id'     => $post['id']?? null,
            'post_type'   => JSONPlaceholder::class,
            'title'       => $post['title']?? null,
            'body'        => $post['body']?? null,
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ];

        if($validator->fails())
        {
            $errors         = $validator->errors();
            $userIdExists   = collect($errors->get('id'))->search('The id has already been taken.');

            if($userIdExists !== false) {
                $this->posts->push($postData);
            }

            return null;
        }

        return $postData;
    }


    /**
     * Validate User Data form API
     * @param array $user
     * @return array|null
     */
    private function validateUserData(array $user): ?array
    {
        $validator = Validator::make($user, [
            'id'        => [
                'required',
                'integer',
                Rule::unique('clients')->where(function ($query) use($user) {
                    return $query->where('client_type', JSONPlaceholder::class)
                        ->where('client_id', $user['id']);
                }),
            ],
            'name'      => 'required|max:100',
            'username'  => 'required|max:50',
            'email'     => 'required|max:50|email|unique:clients,email',
        ]);

        $userData = [
            'client_type' => JSONPlaceholder::class,
            'client_id'   => $user['id']??null,
            'name'        => $user['name']??null,
            'username'    => Str::lower($user['username'])??null,
            'email'       => Str::lower($user['email'])??null,
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ];

        if($validator->fails())
        {
            $errors         = $validator->errors();
            $userIdExists   = collect($errors->get('id'))->search('The id has already been taken.');

            if($userIdExists !== false) {
                $this->users->push($userData);
            }

            return null;
        }

        return $userData;
    }
}
