<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientResource extends JsonResource
{
    /**
     * @OA\Schema(
     *     schema="ClientResource",
     *     description="Client Resource",
     *     required={"id","name","username","email"},
     *      @OA\Property(
     *         property="id",
     *         type="integer",
     *     ),
     *     @OA\Property(
     *         property="name",
     *         type="string",
     *     ),
     *     @OA\Property(
     *         property="username",
     *         type="string",
     *     ),
     *     @OA\Property(
     *         property="email",
     *         type="string",
     *     )
     * )
     *
     */
    public function toArray($request)
    {
        return [
            'id'        => (int) $this->id,
            'name'      => $this->name,
            'username'  => $this->username,
            'email'     => $this->email

        ];
    }
}
