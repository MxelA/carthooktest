<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientPostCommentResource extends JsonResource
{
    /**
     * @OA\Schema(
     *     schema="ClientPostCommentResource",
     *     description="Client Post Resource",
     *     required={"id","title","email","body"},
     *      @OA\Property(
     *         property="id",
     *         type="integer",
     *     ),
     *     @OA\Property(
     *         property="name",
     *         type="string",
     *     ),
     *     @OA\Property(
     *         property="email",
     *         type="string",
     *     ),
     *     @OA\Property(
     *         property="body",
     *         type="string",
     *     )
     * )
     *
     */
    public function toArray($request)
    {
        return [
            'id'        => (int) $this->id,
            'name'      => $this->title,
            'email'     => $this->email,
            'body'      => $this->body,
        ];
    }
}
