<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientPostResource extends JsonResource
{
    /**
     * @OA\Schema(
     *     schema="ClientPostResource",
     *     description="Client Post Resource",
     *     required={"id","title","body"},
     *      @OA\Property(
     *         property="id",
     *         type="integer",
     *     ),
     *     @OA\Property(
     *         property="title",
     *         type="string",
     *     ),
     *     @OA\Property(
     *         property="body",
     *         type="string",
     *     ),
     *     @OA\Property(
     *         property="client",
     *         ref="#/components/schemas/ClientResource"
     *
     *     )
     * )
     *
     */
    public function toArray($request)
    {
        return [
            'id'        => (int) $this->id,
            'title'     => $this->title,
            'body'      => $this->body,
            'client'    => (new ClientResource($this->client))
        ];
    }
}
