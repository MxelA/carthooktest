<?php

namespace App\Http\Controllers\APIv1;



use App\Http\Controllers\Controller;
use App\Http\Resources\ClientPostResource;
use App\Models\ClientPost;
use Illuminate\Http\Request;

class PostController extends Controller
{

    /**
     * * @OA\Get(
     *     path="/api/v1/posts",
     *     summary="Get Posts",
     *     tags={"Post"},
     *     @OA\Parameter(
     *          in="query",
     *          name="filter[title]",
     *          description="Filter by posts title",
     *          required=false,
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(
     *            @OA\Property(
     *              property="data",
     *              required={"data"},
     *              type="array",
     *              @OA\Items(
     *                  ref="#/components/schemas/ClientPostResource"
     *              )
     *            ),
     *            @OA\Property(
     *                  property="meta",
     *                  @OA\Property(
     *                      property="totalItems",
     *                      example="10",
     *                  ),
     *                  @OA\Property(
     *                      property="itemsPerPage",
     *                      example="25",
     *                  ),
     *                  @OA\Property(
     *                      property="lastPage",
     *                      example="1",
     *                  ),
     *                  @OA\Property(
     *                      property="currentPage",
     *                      example="1",
     *                  ),
     *              ),
     *              @OA\Property(
     *                  property="links",
     *                  @OA\Property(
     *                      property="self",
     *                      example="www.link.com",
     *                  ),
     *                  @OA\Property(
     *                      property="first",
     *                      example="www.link.com",
     *                  ),
     *                  @OA\Property(
     *                      property="prev",
     *                      example="www.link.com",
     *                  ),
     *                  @OA\Property(
     *                      property="next",
     *                      example="www.link.com",
     *                  ),
     *                  @OA\Property(
     *                      property="last",
     *                      example="www.link.com",
     *                  ),
     *              ),
     *         )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/ErrorModel")
     *     )
     *  )
     * )
     * */
    public function index(Request $request)
    {
        $clientPostQuery = ClientPost::criteriaByTitle($request->input('filter.title'));

        return ClientPostResource::collection($clientPostQuery->paginate());
    }


    /**
     * * @OA\Get(
     *     path="/api/v1/post/{postId}",
     *     summary="Get Post by ID",
     *     tags={"Post"},
     *     @OA\Parameter(
     *          in="path",
     *          name="postId",
     *          description="Post ID",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(
     *            @OA\Property(
     *              property="data",
     *              required={"data"},
     *              type="array",
     *              @OA\Items(
     *                  ref="#/components/schemas/ClientPostResource"
     *              )
     *            )
     *         )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/ErrorModel")
     *     )
     *  )
     * )
     * */
    public function show($postId)
    {
        $clientPost = ClientPost::with('client')
            ->findOrFail($postId)
        ;

        return new ClientPostResource($clientPost);
    }

    /**
     * * @OA\Get(
     *     path="/api/v1/post/{postId}/comments",
     *     summary="Get Post Comments",
     *     tags={"Post"},
     *     @OA\Parameter(
     *          in="path",
     *          name="postId",
     *          description="Post ID",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(
     *            @OA\Property(
     *              property="data",
     *              required={"data"},
     *              type="array",
     *              @OA\Items(
     *                  ref="#/components/schemas/ClientPostCommentResource"
     *              )
     *            ),
     *            @OA\Property(
     *              ref="#/components/schemas/PaginationMetaData"
     *            )
     *         )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/ErrorModel")
     *     )
     *  )
     * )
     * */
    public function postComments($postId)
    {
        $clientPost = ClientPost::with('client')
            ->findOrFail($postId)
        ;

        return new ClientPostResource($clientPost);
    }
}
