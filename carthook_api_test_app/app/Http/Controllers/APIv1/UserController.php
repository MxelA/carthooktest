<?php

namespace App\Http\Controllers\APIv1;



use App\Http\Controllers\Controller;
use App\Http\Resources\ClientPostResource;
use App\Http\Resources\ClientResource;
use App\Models\Client;
use App\Models\ClientPost;
use App\Services\ThirdPartClients\JSONPlaceholder\JSONPlaceholder;
use Illuminate\Http\Request;


class UserController extends Controller
{

/**
 * * @OA\Get(
 *     path="/api/v1/users",
 *     summary="Get Users",
 *     tags={"User"},
 *     @OA\Parameter(
 *          in="query",
 *          name="filter[email]",
 *          description="Filter by email",
 *          required=false,
 *          @OA\Schema(
 *              type="string",
 *          )
 *      ),
 *     @OA\Parameter(
 *          in="query",
 *          name="filter[name]",
 *          description="Filter by name",
 *          required=false,
 *          @OA\Schema(
 *              type="string",
 *          )
 *      ),
 *     @OA\Response(
 *         response=200,
 *         description="successful operation",
 *         @OA\JsonContent(
 *            @OA\Property(
 *              property="data",
 *              required={"data"},
 *              type="array",
 *              @OA\Items(
 *                  ref="#/components/schemas/ClientResource"
 *              )
 *            ),
 *            @OA\Property(
 *                  property="meta",
 *                  @OA\Property(
 *                      property="totalItems",
 *                      example="10",
 *                  ),
 *                  @OA\Property(
 *                      property="itemsPerPage",
 *                      example="25",
 *                  ),
 *                  @OA\Property(
 *                      property="lastPage",
 *                      example="1",
 *                  ),
 *                  @OA\Property(
 *                      property="currentPage",
 *                      example="1",
 *                  ),
 *              ),
 *              @OA\Property(
 *                  property="links",
 *                  @OA\Property(
 *                      property="self",
 *                      example="www.link.com",
 *                  ),
 *                  @OA\Property(
 *                      property="first",
 *                      example="www.link.com",
 *                  ),
 *                  @OA\Property(
 *                      property="prev",
 *                      example="www.link.com",
 *                  ),
 *                  @OA\Property(
 *                      property="next",
 *                      example="www.link.com",
 *                  ),
 *                  @OA\Property(
 *                      property="last",
 *                      example="www.link.com",
 *                  ),
 *              ),
 *         )
 *     ),
 *     @OA\Response(
 *         response="default",
 *         description="Error",
 *         @OA\JsonContent(ref="#/components/schemas/ErrorModel")
 *     )
 *  )
 * )
 * */
    public function index(Request $request)
    {

        $clientQuery = Client::criteriaByEmail($request->input('filter.email'))
            ->criteriaByName($request->input('filter.name'))
        ;

        return ClientResource::collection($clientQuery->paginate());
    }


    /**
     * * @OA\Get(
     *     path="/api/v1/user/{userId}",
     *     summary="Get User by ID",
     *     tags={"User"},
     *     @OA\Parameter(
     *          in="path",
     *          name="userId",
     *          description="User ID",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(
     *            @OA\Property(
     *              property="data",
     *              required={"data"},
     *              type="array",
     *              @OA\Items(
     *                  ref="#/components/schemas/ClientResource"
     *              )
     *            )
     *         )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/ErrorModel")
     *     )
     *  )
     * )
     * */
    public function show($userId)
    {
        $client = Client::findOrFail($userId);

        return new ClientResource($client);
    }


    /**
     * * @OA\Get(
     *     path="/api/v1/user/{userId}/posts",
     *     summary="Get User Posts",
     *     tags={"User"},
     *     @OA\Parameter(
     *          in="path",
     *          name="userId",
     *          description="User ID",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          in="query",
     *          name="filter[title]",
     *          description="Filter by title",
     *          required=false,
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(
     *            @OA\Property(
     *              property="data",
     *              required={"data"},
     *              type="array",
     *              @OA\Items(
     *                  ref="#/components/schemas/ClientPostResource"
     *              )
     *            ),
     *            @OA\Property(
     *                  property="meta",
     *                  @OA\Property(
     *                      property="totalItems",
     *                      example="10",
     *                  ),
     *                  @OA\Property(
     *                      property="itemsPerPage",
     *                      example="25",
     *                  ),
     *                  @OA\Property(
     *                      property="lastPage",
     *                      example="1",
     *                  ),
     *                  @OA\Property(
     *                      property="currentPage",
     *                      example="1",
     *                  ),
     *              ),
     *              @OA\Property(
     *                  property="links",
     *                  @OA\Property(
     *                      property="self",
     *                      example="www.link.com",
     *                  ),
     *                  @OA\Property(
     *                      property="first",
     *                      example="www.link.com",
     *                  ),
     *                  @OA\Property(
     *                      property="prev",
     *                      example="www.link.com",
     *                  ),
     *                  @OA\Property(
     *                      property="next",
     *                      example="www.link.com",
     *                  ),
     *                  @OA\Property(
     *                      property="last",
     *                      example="www.link.com",
     *                  ),
     *              ),
     *         )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/ErrorModel")
     *     )
     *   )
     * )
     * */
    public function userPosts($userId, Request $request)
    {
        $client     = Client::findOrFail($userId);
        $postQuery  = ClientPost::criteriaByClient($client)
            ->criteriaByTitle($request->input('filter.title'))
            ->orderBy('id', 'ASC')
        ;

        return ClientPostResource::collection($postQuery->paginate());
    }
}
