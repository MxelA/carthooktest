<?php

namespace App\Http\Controllers\APIv1;



use App\Http\Controllers\Controller;
use App\Http\Resources\ClientPostCommentResource;
use App\Http\Resources\ClientPostResource;
use App\Models\ClientPost;
use App\Models\ClientPostComment;
use Illuminate\Http\Request;

class CommentController extends Controller
{

    /**
     * * @OA\Get(
     *     path="/api/v1/comments",
     *     summary="Get Comments",
     *     tags={"Comment"},
     *     @OA\Parameter(
     *          in="query",
     *          name="filter[email]",
     *          description="Filter by posts title",
     *          required=false,
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(
     *            @OA\Property(
     *              property="data",
     *              required={"data"},
     *              type="array",
     *              @OA\Items(
     *                  ref="#/components/schemas/ClientPostCommentResource"
     *              )
     *            ),
     *            @OA\Property(
     *                  property="meta",
     *                  @OA\Property(
     *                      property="totalItems",
     *                      example="10",
     *                  ),
     *                  @OA\Property(
     *                      property="itemsPerPage",
     *                      example="25",
     *                  ),
     *                  @OA\Property(
     *                      property="lastPage",
     *                      example="1",
     *                  ),
     *                  @OA\Property(
     *                      property="currentPage",
     *                      example="1",
     *                  ),
     *              ),
     *              @OA\Property(
     *                  property="links",
     *                  @OA\Property(
     *                      property="self",
     *                      example="www.link.com",
     *                  ),
     *                  @OA\Property(
     *                      property="first",
     *                      example="www.link.com",
     *                  ),
     *                  @OA\Property(
     *                      property="prev",
     *                      example="www.link.com",
     *                  ),
     *                  @OA\Property(
     *                      property="next",
     *                      example="www.link.com",
     *                  ),
     *                  @OA\Property(
     *                      property="last",
     *                      example="www.link.com",
     *                  ),
     *              ),
     *         )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/ErrorModel")
     *     )
     *  )
     * )
     * */
    public function index(Request $request)
    {
        $clientPostQuery = ClientPost::criteriaByTitle($request->input('filter.title'));

        return ClientPostResource::collection($clientPostQuery->paginate());
    }


    /**
     * * @OA\Get(
     *     path="/api/v1/comment/{commentId}",
     *     summary="Get Comment by ID",
     *     tags={"Comment"},
     *     @OA\Parameter(
     *          in="path",
     *          name="commentId",
     *          description="Comment ID",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(
     *            @OA\Property(
     *              property="data",
     *              required={"data"},
     *              type="array",
     *              @OA\Items(
     *                  ref="#/components/schemas/ClientPostCommentResource"
     *              )
     *            )
     *         )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Error",
     *         @OA\JsonContent(ref="#/components/schemas/ErrorModel")
     *     )
     *  )
     * )
     * */
    public function show($commentId)
    {
        $clientPost = ClientPostComment::findOrFail($commentId);

        return new ClientPostCommentResource($clientPost);
    }
}
